var products = [];

function findProduct (productId) {
  return products[findProductKey(productId)];
}

function findProductKey (productId) {
  for (var key = 0; key < products.length; key++) {
    if (products[key].id == productId) {
      return key;
    }
  }
}

var users = [];

function findUser (userId) {
  return users[findUserKey(userId)];
}

function findUserKey (userId) {
  for (var key = 0; key < users.length; key++) {
    if (users[key].id == userId) {
      return key;
    }
  }
}

var productService = {
  findAll(fn) {
    axios
      .get('/api/v1/products')
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  findById(id, fn) {
    axios
      .get('/api/v1/products/' + id)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  create(product, fn) {
    axios
      .post('/api/v1/products', product)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  update(id, product, fn) {
    axios
      .put('/api/v1/products/' + id, product)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  deleteProduct(id, fn) {
    axios
      .delete('/api/v1/products/' + id)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },
}

var userService = {
  findAll(fn) {
    axios
      .get('/api/v1/users')
      .then(response => fn(response))
      .catch(error => console.log(error))
  },
  create(user, fn) {
    axios
      .post('/api/v1/users', user)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  update(id, user, fn) {
    axios
      .put('/api/v1/users/' + id, user)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  deleteUser(id, fn) {
    axios
      .delete('/api/v1/users/' + id)
      .then(response => fn(response))
      .catch(error => console.log(error))
  }
}

var orderService = {
  create(userId, fn) {
    axios
      .get('/api/v1/order/'+userId+'/'+userId+'/1')
      .then(response => fn(response))
      .catch(error => console.log(error))
  },
}

var List = Vue.extend({
  template: '#product-list',
  data: function () {
    return {products: [], searchKey: '', users: []};
  },
  computed: {
    filteredProducts() {
      return this.products.filter((product) => {
      	return product.name.indexOf(this.searchKey) > -1
      	  || product.description.indexOf(this.searchKey) > -1
      	  || product.price.toString().indexOf(this.searchKey) > -1
      })
    },
    userList() {
      return this.users.filter((user) => {
      	return user.name.indexOf(this.searchKey) > -1
      	  //|| user.id.indexOf(this.searchKey) > -1
      })
    },
    addOrder() {
      orderService.create(this.body, r => router.push('/'))
    }
  },
  mounted() {
    productService.findAll(r => {this.products = r.data; products = r.data})
    userService.findAll(r => {this.users = r.data; users = r.data})
  }
});

var Product = Vue.extend({
  template: '#product',
  data: function () {
    return {product: findProduct(this.$route.params.product_id)};
  }
});

var User = Vue.extend({
  template: '#user',
  data: function () {
    return {user: findUser(this.$route.params.user_id)};
  }
});

var ProductEdit = Vue.extend({
  template: '#product-edit',
  data: function () {
    return {product: findProduct(this.$route.params.product_id)};
  },
  methods: {
    updateProduct: function () {
      productService.update(this.product.id, this.product, r => router.push('/'))
    }
  }
});

var ProductDelete = Vue.extend({
  template: '#product-delete',
  data: function () {
    return {product: findProduct(this.$route.params.product_id)};
  },
  methods: {
    deleteProduct: function () {
      productService.deleteProduct(this.product.id, r => router.push('/'))
    }
  }
});


var UserEdit = Vue.extend({
  template: '#user-edit',
  data: function () {
    return {user: findUser(this.$route.params.user_id)};
  },
  methods: {
    updateUser: function () {
      userService.update(this.user.id, this.user, r => router.push('/'))
    }
  }
});

var UserDelete = Vue.extend({
  template: '#user-delete',
  data: function () {
    return {user: findUser(this.$route.params.user_id)};
  },
  methods: {
    deleteUser: function () {
      userService.deleteUser(this.user.id, r => router.push('/'))
    }
  }
});

var AddProduct = Vue.extend({
  template: '#add-product',
  data() {
    return {
      product: {name: '', description: '', price: 0}
    }
  },
  methods: {
    createProduct() {
      productService.create(this.product, r => router.push('/'))
    }
  }
});


var AddUser = Vue.extend({
  template: '#add-user',
  data() {
    return {
      user: {name: ''}
    }
  },
  methods: {
    createUser() {
      userService.create(this.user, r => router.push('/'))
    }
  }
});
var router = new VueRouter({
	routes: [
		{path: '/', component: List},
		{path: '/product/:product_id', component: Product, name: 'product'},
    {path: '/add-product', component: AddProduct},
		{path: '/product/:product_id/edit', component: ProductEdit, name: 'product-edit'},
    {path: '/product/:product_id/delete', component: ProductDelete, name: 'product-delete'},
		{path: '/user/:user_id', component: User, name: 'user'},
    {path: '/add-user', component: AddUser},
    {path: '/user/:user_id/edit', component: UserEdit, name: 'user-edit'},
		{path: '/user/:user_id/delete', component: UserDelete, name: 'user-delete'},
    {path: '/order/:user_id/:product_id/1', name: 'add-order'},
	]
});

new Vue({
  router
}).$mount('#app')
