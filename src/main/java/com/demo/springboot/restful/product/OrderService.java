package com.demo.springboot.restful.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;

    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }

    public Order save(Order stock) {
        return orderRepository.save(stock);
    }

    public void deleteById(Long id) {
        orderRepository.deleteById(id);
    }

	public void createOrder(Long userId, Long productId, Integer num) {
        User user = userRepository.findById(userId).orElse(null);
        Product product = productRepository.findById(productId).orElse(null);

        Order order = new Order();
        order.setUser(user);

        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setOrder(order);
        orderDetail.setProduct(product);

        orderDetail.setNumber(num);

        order.orderdetails.add(orderDetail);

        orderRepository.save(order);
	}
}
