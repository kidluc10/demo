  
package com.demo.springboot.restful.product;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;

@Controller
public class OrderController {
	final static Logger log = Logger.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @GetMapping("/api/v1/order/{user_id}/{productId}/{num}")
    public ResponseEntity<String> create(@PathVariable(name = "user_id") Long userId, @PathVariable Long productId, @PathVariable Integer num) {
        orderService.createOrder(userId, productId, num);
        return ResponseEntity.ok("Success");
    }
}

