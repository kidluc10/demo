package com.demo.springboot.restful.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public List<User> findAll() {
        List<User> list = userRepository.findAll();
        return list;
    }

    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    public User save(User stock) {
        return userRepository.save(stock);
    }

    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }
}
